package ictgradschool.industry.io.ex05;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class PCPlayer extends Player{
    //PCPlayer has a scanner to read file
    private Scanner sc;

    /**
     * constructor of PCPlayer
     * @param filePath the path to the file
     */
    public PCPlayer(String filePath) {
        //call super() first
        super();
        try {
            //set up the scanner
            this.sc = new Scanner(new File(filePath));
        } catch (IOException e) {
            System.err.println(e);
        }finally {
            //generate the serect code for computer player
            this.setCode(generateCode());
        }
    }

    /**
     * generateCode() is used for two proposes
     * one is to generate the initial code for computer player
     * one is to generate code for guessing human player's secret code when there is no more code in file
     * @return 4-digit code
     */
    private char[] generateCode(){
        //variable to return
        char[] temp = new char[4];
        //Use array list to maintain the functionality of generating no duplicated digit
        ArrayList<Character> arrayList = new ArrayList<Character>(Arrays.asList('0','1','2','3','4','5','6','7','8','9'));
        //A for loop to generate 4-digit code
        for(int i=0;i<4;i++){
            //Randomly pick up a number from array list
            int index = (int) Math.floor(Math.random()*arrayList.size());
            //Insert to returning array
            temp[i] = arrayList.get(index);
            //Remove from array list
            arrayList.remove(index);
        }
        //Return the array
        return temp;
    }

    /**
     * readNext() is to read next code from file
     * @return next code from file
     */
    public char[] readNext() {
        if(this.sc==null)
            return generateCode();
        if (this.sc.hasNext())
            return this.sc.nextLine().toCharArray();
        return generateCode();
    }


}
