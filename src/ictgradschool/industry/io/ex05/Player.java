package ictgradschool.industry.io.ex05;

public class Player {
    //Two fields in Player class
    //One is code which is a character array
    //One is lose which is a boolean to record
    private char[] code;
    private boolean lose;

    //Empty constructor
    Player(){
    }

    /**
     * Constructor with a pre-defined code
     * @param code pre-defined code by human player
     */
    public Player(char[] code) {
        this.code = code;
        this.lose=false;
    }

    /**
     * check() method is to check the bulls and cows
     * @param code input code
     */
    public void check(char[] code){
        //set local variables of bulls and cows
        int bulls=0, cows=0;
        //use a for loop to check though code
        for(int i=0;i<4;i++){
            //calculate bulls
            if(this.code[i] == code[i]){
                bulls++;
            }
            //calculate cows
            for(char c1:code){
                if(this.code[i] == c1)
                    cows++;
            }
        }
        //Output the notice of bulls and cows
        System.out.println("Bulls: "+bulls+" Cows: "+cows);
        //If the bulls and cows are all four
        if(bulls == 4 && cows == 4)
            //then this play lost the game
            this.lose=true;
    }

    /**
     * getter of lose field
     * @return boolean of lose
     */
    public boolean isLose() {
        return lose;
    }

    /**
     * setter of code field
     * @param code the code to be set
     */
    public void setCode(char[] code) {
        this.code = code;
    }
}
