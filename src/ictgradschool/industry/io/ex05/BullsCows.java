package ictgradschool.industry.io.ex05;

import ictgradschool.Keyboard;

public class BullsCows {
    //There are three fields which are man, pc and restart flag.
    //man is human player
    //pc is computer player
    //restart is a boolean flag when one of players win and ask if want to re-begin a new game
    private Player man;
    private Player pc;
    private boolean restart;

    /**
     * Main programme entry
     * @param args in this case, args is useless
     */
    public static void main(String[] args){
        //Initialise BullsCows class
        BullsCows bc = new BullsCows();
        //Call run method to start game
        bc.run();
    }

    /**
     * Constructor of BullsCows class
     * Only set up the restart flag to be false
     * And use init() method to set up players
     */
    private BullsCows() {
        //set restart flag to be false at most beginning
        this.restart=false;
        //call init() to initialise players
        this.init();
    }


    /**
     *init() method is going to set up all players, both human and computer players
     */
    private void init(){
        //initialise computer player
        this.pc = new PCPlayer("files/pc.txt");//TODO Fix the path
        //system prints out notice for human player to enter a secret code
        System.out.print("Please enter your secret code:");
        //Receive the code from console
        char[] playerCode =  Keyboard.readInput().toCharArray();
        //Validate the code
        while(!validateCode(playerCode)){
            //If it is not okay, ask player to re-type it
            System.out.print("Invalid code, re-enter please:");
            //get the code again
            playerCode =  Keyboard.readInput().toCharArray();
        }
        //Once the code is okay, the game should start
        System.out.println("==========GAME START==========");
        //Initialise the human player
        this.man = new Player(playerCode);
    }

    /**
     *validate the input code from user
     * @param code the input code from user though console
     * @return validation of true or false
     */
    private boolean validateCode(char[] code){
        //firstly check the length of code, if should be 4
        if(code.length!=4)
            return false;
        //then check if the code is all digits
        for(int i=0;i<4;i++){
            //Here use ASCII to check if the character is digit
            //Or use Character.isDigit() method should be okay
            if(code[i]<48 || code[i]>57)
                return false;
            //then check if there are duplicated digits
            for(int j=i+1;j<4;j++){
                if(code[i]==code[j])
                    return false;
            }
        }
        return true;
    }

    /**
     *run() method is the main running method to maintain the logic of game
     */
    private void run(){
        //Set the round count from 1
        int roundCount = 1;
        //Down-case the computer player
        PCPlayer pcp =(PCPlayer) this.pc;
        //A while true loop
        while(true){
            //Output the round number
            System.out.println("==============================\nRound: "+roundCount);
            //Check the round count to know who is guessing
            if(roundCount%2 == 1){
                //update round count
                roundCount++;
                //Output notice
                System.out.print("Your Guess: ");
                //Receive the input
                char[] temp = Keyboard.readInput().toCharArray();
                //Check the input
                while(!validateCode(temp)) {
                    System.out.print("Invalid code, re-enter please: ");
                    temp = Keyboard.readInput().toCharArray();
                }
                //Once the input is okay, call check method to check the code
                this.pc.check(temp);
                //If the combat player lose
                if (this.pc.isLose()) {
                    //If so, output the notice and ask if want to restart
                    System.out.println("==============================\nGreat! You Win! Restart? Y/N.");
                    //Receive input
                    String res = Keyboard.readInput();
                    //Check if it is Y
                    if (res.equals("Y")) {
                        //If so, set restart flag to be true
                        this.restart = true;
                    }
                    //Otherwise, break the while loop
                    break;
                }
            }else{
                //update round count
                roundCount++;
                //Output computer guessing
                System.out.print("Computer Guess: ");
                //call readNext() method to read next guessing
                char[] temp = pcp.readNext();
                //Output computer guessing
                System.out.println(temp);
                //call player check method to check this guessing
                this.man.check(temp);
                //If the combat player lose
                if(this.man.isLose()){
                    //Ask player to restart or not
                    System.out.println("==============================\nSorry! Computer Wins! Restart? Y/N");
                    String res = Keyboard.readInput();
                    if(res.equals("Y")){
                        this.restart=true;
                    }
                    break;
                }
            }
        }
        //If restart this game
        if(this.restart) {
            //then go to init() to renew the game settings
            init();
            //and last run again
            run();
        }
    }
}
